# [4.4.0](https://gitlab.com/to-be-continuous/robotframework/compare/4.3.0...4.4.0) (2025-01-27)


### Features

* disable tracking service by default ([35ffa91](https://gitlab.com/to-be-continuous/robotframework/commit/35ffa912cfcc3cc2d9aa656bc45fdc60d4d39010))

# [4.3.0](https://gitlab.com/to-be-continuous/robotframework/compare/4.2.2...4.3.0) (2024-05-06)


### Features

* add hook scripts support ([05c4c08](https://gitlab.com/to-be-continuous/robotframework/commit/05c4c08673f6bd0695acbcb91712c1369229618a))

## [4.2.2](https://gitlab.com/to-be-continuous/robotframework/compare/4.2.1...4.2.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([594ca82](https://gitlab.com/to-be-continuous/robotframework/commit/594ca827597dece275d85bbce9a5625665aa351f))

## [4.2.1](https://gitlab.com/to-be-continuous/robotframework/compare/4.2.0...4.2.1) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([e7e896b](https://gitlab.com/to-be-continuous/robotframework/commit/e7e896b68fbfc49a5167201eb934dfaa83998d64))

# [4.2.0](https://gitlab.com/to-be-continuous/robotframework/compare/4.1.0...4.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([7d08f91](https://gitlab.com/to-be-continuous/robotframework/commit/7d08f9115a5e14b139619ed9bef0403013a0b164))

# [4.1.0](https://gitlab.com/to-be-continuous/robotframework/compare/4.0.1...4.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([3adee38](https://gitlab.com/to-be-continuous/robotframework/commit/3adee38e721ce8eb63bcbf5a4e73b7274d1a1743))

## [4.0.1](https://gitlab.com/to-be-continuous/robotframework/compare/4.0.0...4.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([7d7c7a0](https://gitlab.com/to-be-continuous/robotframework/commit/7d7c7a0eb5bd3b28493cd4ff119c46754e0d1cb9))

# [4.0.0](https://gitlab.com/to-be-continuous/robotframework/compare/3.3.0...4.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([a552912](https://gitlab.com/to-be-continuous/robotframework/commit/a55291296d79486c0cb3e8b94fb1cc294b03e666))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

# [3.3.0](https://gitlab.com/to-be-continuous/robotframework/compare/3.2.1...3.3.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([15748f6](https://gitlab.com/to-be-continuous/robotframework/commit/15748f683ae4c13b219f612eff20ed13b19e1501))

## [3.2.1](https://gitlab.com/to-be-continuous/robotframework/compare/3.2.0...3.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([5b032f8](https://gitlab.com/to-be-continuous/robotframework/commit/5b032f8ce1acd50be89b99a752f2f57c4ef576df))

# [3.2.0](https://gitlab.com/to-be-continuous/robotframework/compare/3.1.0...3.2.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([4e87a64](https://gitlab.com/to-be-continuous/robotframework/commit/4e87a64458dceea6287933f2025e3f14a5379400))

# [3.1.0](https://gitlab.com/to-be-continuous/robotframework/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([645ff6a](https://gitlab.com/to-be-continuous/robotframework/commit/645ff6a16078a22ba84c0c77a6430d4a6f0a9acb))

# [3.0.0](https://gitlab.com/to-be-continuous/robotframework/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([8c966c8](https://gitlab.com/to-be-continuous/robotframework/commit/8c966c88f97a8d11fd1039bd87cd6b8657a42d95))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/robotframework/compare/2.0.2...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([90725d2](https://gitlab.com/to-be-continuous/robotframework/commit/90725d27f462c59332a1dd788c73a53e10e09546))

## [2.0.2](https://gitlab.com/to-be-continuous/robotframework/compare/2.0.1...2.0.2) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([774be87](https://gitlab.com/to-be-continuous/robotframework/commit/774be87b5383a9dcc2afa7f82cb19c6b81bedded))

## [2.0.1](https://gitlab.com/to-be-continuous/robotframework/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([9d4b52f](https://gitlab.com/to-be-continuous/robotframework/commit/9d4b52fd1a63eeea58d90624ebc521637cd4e7e2))

## [2.0.0](https://gitlab.com/to-be-continuous/robotframework/compare/1.3.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([59a9008](https://gitlab.com/to-be-continuous/robotframework/commit/59a90089d5cca2c45edd2efc57f2c837c425fb9f))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.0](https://gitlab.com/to-be-continuous/robotframework/compare/1.2.0...1.3.0) (2021-07-28)

### Features

* add Vault variant ([4b398fc](https://gitlab.com/to-be-continuous/robotframework/commit/4b398fcfebd18313ae3acbe77116666c2a1704c3))

## [1.2.0](https://gitlab.com/to-be-continuous/robotframework/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([a38c32e](https://gitlab.com/to-be-continuous/robotframework/commit/a38c32edad87339367d87abb401eb2a148ec0a4e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/robotframework/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([eeb4ba0](https://gitlab.com/Orange-OpenSource/tbc/robotframework/commit/eeb4ba0232c061c6b2ff85302b61a638e26e8be4))

## 1.0.0 (2021-05-10)

### Features

* initial release ([d37bd96](https://gitlab.com/Orange-OpenSource/tbc/robotframework/commit/d37bd967685685de364e6503324e28d01c1947bd))
